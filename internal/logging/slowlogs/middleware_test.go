package slowlogs

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// NewTestLogger returns a LoggerFunc that uses a test logger (with an in-memory hook)
func NewTestLogger() (LoggerFunc, *testlog.Hook) {
	logger, hook := testlog.NewNullLogger()

	return func(r *http.Request) *logrus.Entry {
		return logger.WithFields(logrus.Fields{})
	}, hook
}

func TestMiddlewareLogsSlowRequests(t *testing.T) {
	t.Parallel()
	slowHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	loggerFunc, hook := NewTestLogger()

	middleware := NewMiddleware(slowHandler, loggerFunc, 0)

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rr := httptest.NewRecorder()
	middleware.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code, "Expected HTTP status OK")

	lastEntry := hook.LastEntry()
	require.NotNil(t, lastEntry, "Expected a log entry for slow requests")

	require.Contains(t, lastEntry.Data, "middleware_time_distribution", "Expected 'execution_time_distribution' in log entry")
	require.Contains(t, lastEntry.Data, "total_execution_time_ms", "Expected 'total_execution_time_ms' in log entry")

	totalExecutionTime := lastEntry.Data["total_execution_time_ms"].(int64)
	assert.GreaterOrEqual(t, totalExecutionTime, int64(0), "Expected 'total_execution_time_ms' to be greater than or equal to 0 milliseconds")
}

func TestMiddlewareDoesNotLogFastRequests(t *testing.T) {
	t.Parallel()
	fastHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	loggerFunc, hook := NewTestLogger()

	middleware := NewMiddleware(fastHandler, loggerFunc, 500*time.Millisecond)

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rr := httptest.NewRecorder()

	middleware.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)

	lastEntry := hook.LastEntry()
	require.Nil(t, lastEntry, "Log entry is not expected")
}

func TestPrepareMiddlewareTimingInfoInMS(t *testing.T) {
	t.Parallel()
	timings := map[string]time.Duration{
		"handler1": 150 * time.Millisecond,
		"handler2": 200 * time.Millisecond,
	}

	ctx := context.WithValue(context.Background(), timingContextKey, &timingInfo{
		MiddlewareTimings: timings,
	})

	timingsInMS := prepareMiddlewareTimingInfoInMS(ctx)

	require.NotNil(t, timingsInMS)
	assert.Equal(t, int64(150), timingsInMS["handler1_ms"])
	assert.Equal(t, int64(200), timingsInMS["handler2_ms"])
}

// TestLogHandlerTimingWithChainedHandlers tests LogHandlerTiming with handler chaining.
func TestLogHandlerTimingWithChainedHandlers(t *testing.T) {
	t.Parallel()
	slowHandler := func() http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			time.Sleep(5 * time.Millisecond)
			w.WriteHeader(http.StatusOK)
		})
	}

	anotherSlowHandler := func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			time.Sleep(10 * time.Millisecond)
			handler.ServeHTTP(w, r)
		})
	}

	loggerFunc, hook := NewTestLogger()

	chainedHandler := LogHandlerTiming(slowHandler(), "slowHandler")
	chainedHandler = LogHandlerTiming(anotherSlowHandler(chainedHandler), "anotherSlowHandler")
	chainedHandler = NewMiddleware(chainedHandler, loggerFunc, 0)

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rr := httptest.NewRecorder()

	chainedHandler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)

	require.NotNil(t, hook.LastEntry())
	require.NotNil(t, hook.LastEntry().Data["middleware_time_distribution"])

	executionTimes := hook.LastEntry().Data["middleware_time_distribution"].(map[string]int64)
	handler1Duration, ok := executionTimes["slowHandler_ms"]
	require.True(t, ok)
	handler2Duration, ok := executionTimes["anotherSlowHandler_ms"]
	require.True(t, ok)
	totalExecutionTimeMS, ok := hook.LastEntry().Data["total_execution_time_ms"].(int64)
	require.True(t, ok)

	// Assert that the first handler execution time is approximately 300 milliseconds
	assert.GreaterOrEqual(t, handler1Duration, int64(5))
	assert.Less(t, handler1Duration, int64(20)) // Allow some margin

	// Assert that the second handler execution time is approximately 400 milliseconds
	assert.GreaterOrEqual(t, handler2Duration, int64(10))
	assert.Less(t, handler2Duration, int64(25)) // Allow some margin

	assert.GreaterOrEqual(t, totalExecutionTimeMS, int64(15))
	assert.Less(t, totalExecutionTimeMS, int64(30)) // Allow some margin
}
