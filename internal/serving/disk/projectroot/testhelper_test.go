package projectroot

import (
	"testing"

	"gitlab.com/gitlab-org/gitlab-pages/internal/testhelpers"
)

func TestMain(m *testing.M) {
	testhelpers.Run(m)
}
